<?php
  session_start();
  include('../config/connect.php');
  if(!isset($_SESSION['logsuccess'])) {
    header("Location: login.php");
  }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trang quản trị</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">CASL</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="tintuc.php">Tin tức</a></li>
            <li><a href="togiac.php">Tin tố giác</a></li>
            <li><a href="anninh.php">Trật tự - An ninh</a></li>
            <li><a href="vbhanhchinh.php">Văn bản hành chính</a></li>
            <li><a href="hoidap.php">Hỏi đáp</a></li>
            <li><a href="binhluan.php">Bình luận</a></li>
            <li><a href="lienhe.php">Liên hệ</a></li>
            <?php
              if(isset($_SESSION['role']))
              {
                if($_SESSION['role'] == 1) {
                  echo '<li><a href="taikhoan.php">Tài khoản</a></li>';
                }
              }
            ?>
            <li><a href="#"><b>Hi, <?=$_SESSION['name']?></b></a></li>
            <li><a href="dangxuat.php">Đăng xuất</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h1>Tiếp nhận xử lý tố giác tội phạm</h1>
      </div>
      <p class="lead">Ngày 19/8/1945, Đảng Cộng sản Việt Nam đã lãnh đạo các lực lượng vũ trang cách mạng và nhân dân cả nước tiến hành thắng lợi cuộc khởi nghĩa giành chính quyền, lập nên nước Việt Nam dân chủ cộng hòa – Nhà nước công nông đầu tiên ở Đông Nam Á và ngày 19/8/1945 cũng là ngày lực lượng Công an nhân dân Việt Nam ra đời. Trong cao trào cách mạng chung của cả nước, lực lượng vũ trang và nhân dân các dân tộc tỉnh Sơn La đã khởi nghĩa giành chính quyền thắng lợi vào ngày 26/8/1945; cùng với sự ra đời của chính quyền cách mạng, ngày 10/10/1945, Ty Liêm phóng Sơn La – tổ chức đầu tiên của lực lượng Công an nhân dân Sơn La chính thức được thành lập.</p>
    </div>

    <footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
