<?php
session_start();
  if(!isset($_SESSION['logsuccess'])) {
    if(isset($_SESSION['role'])) {
      if($_SESSION['role'] != 1)
        header("Location: login.php");
    }
  }
  include('../config/connect.php');

  if(isset($_POST['themtk']))
  {
    $hoten = $_POST['hotenadd'];
    $email = $_POST['emailadd'];
    $quyen = $_POST['quyenadd'];
    $matkhau = $_POST['matkhauadd'];
    $matkhau = md5($matkhau);

    $sql = "INSERT INTO users(name, email, role, password) VALUES('$hoten', '$email', '$quyen', '$matkhau')";
    $result = $conn->query($sql);

    if($result)
    {
      $_SESSION['taikhoan'] = "Thêm tài khoản thành công!";
    } else {
      $_SESSION['taikhoanfalse'] = "Có lỗi xảy ra khi thêm tài khoản vui lòng thử lại!";
    }
  }

  if(isset($_POST['suatk']))
  {
    $id = $_POST['idupdate'];
    $hoten = $_POST['hotenedit'];
    $email = $_POST['emailedit'];
    $quyen = $_POST['quyenedit'];
    $matkhau = $_POST['matkhauedit'];
    $matkhau = md5($matkhau);

    $sql = "UPDATE users SET name = '$hoten', email = '$email', role = '$quyen', password = '$matkhau' WHERE id = '$id'";
    $result = $conn->query($sql);

    if($result)
    {
      $_SESSION['update'] = "Cập nhật tài khoản thành công!";
    } else {
      $_SESSION['updatefalse'] = "Có lỗi xảy ra khi cập nhật tài khoản vui lòng thử lại!";
    }
  }

  if(isset($_POST['xoatk']))
  {
    $idxoa = $_POST['idxoa'];

    $sql = "DELETE FROM users WHERE id = '$idxoa'";
    $result = $conn->query($sql);

    if($result)
    {
      $_SESSION['xoa'] = "Xóa tài khoản thành công!";
    } else {
      $_SESSION['xoafalse'] = "Có lỗi xảy ra khi xóa tài khoản vui lòng thử lại!";
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trang quản trị</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">CASL</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="tintuc.php">Tin tức</a></li>
            <li><a href="togiac.php">Tin tố giác</a></li>
            <li><a href="anninh.php">Trật tự - An ninh</a></li>
            <li><a href="vbhanhchinh.php">Văn bản</a></li>
            <li><a href="hoidap.php">Hỏi đáp pháp luật</a></li>
            <li><a href="binhluan.php">Bình luận</a></li>
            <li><a href="lienhe.php">Liên hệ</a></li>
            <li class="active"><a href="taikhoan.php">Tài khoản</a></li>
            <li><a href="#"><b>Hi, <?=$_SESSION['name']?></b></a></li>
            <li><a href="dangxuat.php">Đăng xuất</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <?php
          if(isset($_SESSION['taikhoan'])) {
            unset($_SESSION['taikhoanfalse']);
            echo '<div class="alert alert-success">' . $_SESSION['taikhoan'] . '</div>';
          }
          if(isset($_SESSION['taikhoanfalse'])) {
            unset($_SESSION['taikhoan']);
            echo '<div class="alert alert-danger">' . $_SESSION['taikhoanfalse'] . '</div>';
          }

          if(isset($_SESSION['update'])) {
            unset($_SESSION['updatefalse']);
            unset($_SESSION['taikhoan']);
            echo '<div class="alert alert-success">' . $_SESSION['update'] . '</div>';
          }

          if(isset($_SESSION['updatefalse'])) {
            unset($_SESSION['taikhoan']);
            unset($_SESSION['update']);
            echo '<div class="alert alert-danger">' . $_SESSION['updatefalse'] . '</div>';
          }

          if(isset($_SESSION['xoa'])) {
            unset($_SESSION['taikhoan']);
            unset($_SESSION['update']);
            unset($_SESSION['xoafalse']);
            echo '<div class="alert alert-info">' . $_SESSION['xoa'] . '</div>';
          }

          if(isset($_SESSION['xoafalse'])) {
            unset($_SESSION['taikhoan']);
            unset($_SESSION['update']);
            unset($_SESSION['xoa']);
            echo '<div class="alert alert-danger">' . $_SESSION['xoafalse'] . '</div>';
          }
        ?>
        <h1>Quản lý tài khoản biên tập viên</h1>
        <input type="submit" class="btn btn-info dt-add" name="them" value="Thêm tài khoản">
      </div>
      <table id="table" class="display table table-bordered table-hover">
       <thead>
          <tr>
             <th>ID</th>
             <th>Họ và tên</th>
             <th>Email</th>
             <th>Quyền hạn</th>
             <th>Mật khẩu</th>
             <th>Thao tác</th>
          </tr>
       </thead>
       <tbody>
          <?php
            $sql = "SELECT * FROM users";
            $result = $conn->query($sql);
            while($users = $result->fetch_assoc())
              {
          ?>
          <tr>
             <th scope="row"><?=$users['id']?></th>
             <td><?=$users['name']?></td>
             <td><?=$users['email']?></td>
             <td><?php echo $users['role'] == 0 ? "Biên tập viên" : "Quản trị viên";?></td>
             <td><?=$users['password']?></td>
             <td><a href="#" class="dt-edit">Sửa</a> - <a href="#" class="dt-remove">Xóa</a></td>
          </tr>
          <?php
            }
          ?>
       </tbody>
      </table>
    </div>

    <footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>

    <!-- Modal -->
    <div id="modalEdit" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <input type="hidden" class="form-control disabled" name="idupdate" id="idupdate">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Họ và tên</label>
                <input type="text" class="form-control" name="hotenedit" id="hotenedit">
              </div>
               <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" class="form-control" name="emailedit" id="emailedit">
              </div>
              <div class="form-group">
                <label for="sel1">Quyền hạn</label>
                <select name="quyenedit" class="form-control" id="quyenedit">
                  <option value="0" selected>Biên tập viên</option>
                  <option value="1">Quản trị viên</option>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Mật khẩu</label>
                <input type="text" class="form-control" name="matkhauedit" id="matkhauedit">
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="suatk" value="Cập nhật">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <input type="hidden" class="form-control disabled" name="id" id="id">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Họ và tên</label>
                <input type="text" class="form-control" name="hotenadd" id="hotenadd">
              </div>
               <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" class="form-control" name="emailadd" id="emailadd">
              </div>
              <div class="form-group">
                <label for="sel1">Quyền hạn</label>
                <select name="quyenadd" class="form-control" id="quyenadd">
                  <option value="0" selected>Biên tập viên</option>
                  <option value="1">Quản trị viên</option>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Mật khẩu</label>
                <input type="text" class="form-control" name="matkhauadd" id="matkhauadd">
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="themtk" value="Thêm tài khoản">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalRemove" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <input type="hidden" class="form-control disabled" name="idxoa" id="idxoa">
                <label for="exampleInputEmail1">Bạn chắc chắn muốn xóa tài khoản này?</label>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-danger" name="xoatk" value="Xóa tài khoản">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#table').DataTable();
      });

      //Edit row buttons
      $('.dt-edit').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idupdate').val(dtRow[0].cells[0].innerHTML);
            $('#hotenedit').val(dtRow[0].cells[1].innerHTML);
            $('#emailedit').val(dtRow[0].cells[2].innerHTML);
            $('#matkhauedit').val(dtRow[0].cells[4].innerHTML);
            console.log(dtRow[0].cells[i].innerHTML);
          }
          $('#modalEdit').modal('show');
        });
      });

      //add row buttons
      $('.dt-add').each(function () {
        $(this).on('click', function(evt){
          $('#modalAdd').modal('show');
        });
      });

      //add row buttons
      $('.dt-remove').each(function () {
        $(this).on('click', function(evt){
          $('#modalRemove').modal('show');
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idxoa').val(dtRow[0].cells[0].innerHTML);
            console.log(dtRow[0].cells[i].innerHTML);
          }
        });
      });
    </script>
  </body>
</html>
