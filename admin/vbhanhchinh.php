<?php
session_start();
  if(!isset($_SESSION['logsuccess'])) {
    header("Location: login.php");
  }
include('../config/connect.php');
if(isset($_POST['submit'])) {
  $id = $_POST['id'];
  $tieude = $_POST['tieude'];
  $noidung = $_POST['noidung'];


  //Thư mục bạn sẽ lưu file upload
  $target_dir    = "../images/uploads/";
  //Vị trí file lưu tạm trong server
  $target_file   = $target_dir . basename($_FILES["fileupload"]["name"]);
  $allowUpload   = true;
  //Lấy phần mở rộng của file
  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
  $maxfilesize   = 800000; //(bytes)
  ////Những loại file được phép upload
  $allowtypes    = array('jpg', 'png', 'jpeg', 'gif', 'pdf', 'doc', 'docx', 'pps', 'ppt', 'xls');


  //Kiểm tra xem có phải là ảnh
  @$check = true;
  if($check !== false) {
      // echo "Đây là file ảnh - " . $check["mime"] . ".";
      $allowUpload = true;
  } else {
      $_SESSION['error'] = "Không phải file ảnh vui lòng kiểm tra lại.";
      $allowUpload = false;
  }

  // Kiểm tra nếu file đã tồn tại thì không cho phép ghi đè
  if (file_exists($target_file)) {
      $_SESSION['error'] = "File đã tồn tại trên hệ thống.";
      $allowUpload = false;
  }
  // Kiểm tra kích thước file upload cho vượt quá giới hạn cho phép
  if ($_FILES["fileupload"]["size"] > $maxfilesize)
  {
      $_SESSION['error'] = "Không được upload ảnh lớn hơn $maxfilesize (bytes).";
      $allowUpload = false;
  }


  // Kiểm tra kiểu file
  if (!in_array($imageFileType,$allowtypes ))
  {
      $_SESSION['error'] = "Chỉ được upload các định dạng JPG, PNG, JPEG, GIF";
      $allowUpload = false;
  }

  // Check if $uploadOk is set to 0 by an error
  if ($allowUpload) {
      if (move_uploaded_file($_FILES["fileupload"]["tmp_name"], $target_file))
      {
        //upload image success
        $hinhanh = $_FILES["fileupload"]["name"];
        $sql = "UPDATE vbhanhchinh SET name = '$tieude', content = '$noidung', image = '$hinhanh'";
        $result = $conn->query($sql);
        if($result) {
          if(isset($_SESSION['error']))
            unset($_SESSION['error']);
          if(isset($_SESSION['falseupdate']))
            unset($_SESSION['falseupdate']);
          $_SESSION['success'] = "Cập nhật văn bản hành chính thành công!";
        }
      }
      else
      {
          $_SESSION['falseupdate'] = "Lỗi cập nhật vui lòng thử lại!";
      }
  }
  else
  {
      $_SESSION['error'] = "Không upload được file, vui lòng chọn file và thử lại!";
  }
}


//xoa tin
if(isset($_POST['xoa'])) {
  $idXoa = $_POST['idxoa'];
  $sql = "DELETE FROM vbhanhchinh WHERE id = '$idXoa'";
  $resultXoa = $conn->query($sql);

  if($resultXoa)
  {
    $sql1 = "set @autoid :=0";
    $sql2 = "update vbhanhchinh set id = @autoid := (@autoid+1)";
    $sql3 = "alter table vbhanhchinh Auto_Increment = 1";
    $conn->query($sql1);
    $conn->query($sql2);
    $conn->query($sql3);
    $_SESSION['xoa'] = "Xóa thành công văn bản hành chính";
  } else {
    $_SESSION['xoa'] = "Xóa không thành công văn bản hành chính!";
  }
}

//them tin
if(isset($_POST['them'])) {
  $tieude = $_POST['tieudeadd'];
  $noidung = $_POST['noidungadd'];

  //Thư mục bạn sẽ lưu file upload
  $target_dir    = "../images/uploads/";
  //Vị trí file lưu tạm trong server
  $target_file   = $target_dir . basename($_FILES["uploadFile"]["name"]);
  $allowUpload   = true;
  //Lấy phần mở rộng của file
  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
  $maxfilesize   = 8000000; //(bytes)
  ////Những loại file được phép upload
  $allowtypes    = array('jpg', 'png', 'jpeg', 'gif', 'pdf', 'doc', 'docx', 'pps', 'ppt', 'xls');


  //Kiểm tra xem có phải là ảnh
  @$check = true;
  if($check !== false) {
      // echo "Đây là file ảnh - " . $check["mime"] . ".";
      $allowUpload = true;
  } else {
      $_SESSION['error'] = "Không phải file ảnh vui lòng kiểm tra lại.";
      $allowUpload = false;
  }

  // Kiểm tra nếu file đã tồn tại thì không cho phép ghi đè
  if (file_exists($target_file)) {
      $_SESSION['error'] = "File đã tồn tại trên hệ thống.";
      $allowUpload = false;
  }
  // Kiểm tra kích thước file upload cho vượt quá giới hạn cho phép
  if ($_FILES["uploadFile"]["size"] > $maxfilesize)
  {
      $_SESSION['error'] = "Không được upload ảnh lớn hơn $maxfilesize (bytes).";
      $allowUpload = false;
  }


  // Kiểm tra kiểu file
  if (!in_array($imageFileType,$allowtypes ))
  {
      $_SESSION['error'] = "Chỉ được upload các định dạng JPG, PNG, JPEG, GIF";
      $allowUpload = false;
  }

  // Check if $uploadOk is set to 0 by an error
  if ($allowUpload) {
      if (move_uploaded_file($_FILES["uploadFile"]["tmp_name"], $target_file))
      {
        //upload image success
        $hinhanh = $_FILES["uploadFile"]["name"];
        $sql = "INSERT INTO vbhanhchinh(name, image, content) VALUES ('$tieude', '$hinhanh', '$noidung')";
        $result = $conn->query($sql);
        if($result) {
          if(isset($_SESSION['error']))
            unset($_SESSION['error']);
          if(isset($_SESSION['falseupdate']))
            unset($_SESSION['falseupdate']);
          $_SESSION['success'] = "Thêm văn bản hành chính thành công!";
        }
      }
      else
      {
          $_SESSION['falseupdate'] = "Lỗi thêm mới vui lòng thử lại!";
      }
  }
  else
  {
      $_SESSION['error'] = "Không upload được file, vui lòng chọn file và thử lại!";
  }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trang quản trị</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <style>
      .tablee-cell {
        display: block;
        white-space: nowrap;
        width: 200px;
        height: 10px;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    </style>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">CASL</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Trang chủ</a></li>
            <li><a href="tintuc.php">Quản lý tin tức</a></li>
            <li><a href="togiac.php">Tin tố giác</a></li>
            <li><a href="anninh.php">Trật tự - An ninh</a></li>
            <li class="active"><a href="vbhanhchinh.php">Văn bản hành chính</a></li>
            <li><a href="hoidap.php">Hỏi đáp pháp luật</a></li>
            <li><a href="binhluan.php">Bình luận</a></li>
            <li><a href="lienhe.php">Liên hệ</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <?php
                if(isset($_SESSION['error'])) {
                  if(isset($_SESSION['success']))
                    unset($_SESSION['success']);
                  echo '<div class="alert alert-danger">' . $_SESSION['error'] . '</div>';
                }
                if(isset($_SESSION['success'])) {
                  if(isset($_SESSION['error']))
                    unset($_SESSION['error']);
                  if(isset($_SESSION['falseupdate']))
                    unset($_SESSION['falseupdate']);
                  echo '<div class="alert alert-success">' . $_SESSION['success'] . '</div>';
                }
                if(isset($_SESSION['falseupdate'])) {
                  if(isset($_SESSION['success']))
                    unset($_SESSION['success']);
                  echo '<div class="alert alert-danger">' . $_SESSION['falseupdate'] . '</div>';
                }

                if(isset($_SESSION['xoa'])) {
                  if(isset($_SESSION['success']))
                    unset($_SESSION['success']);
                  if(isset($_SESSION['error']))
                    unset($_SESSION['error']);
                  echo '<div class="alert alert-success">' . $_SESSION['xoa'] . '</div>';
                }
              ?>
        <h2>Văn bản hành chính</h2>
        <input type="submit" class="btn btn-success dt-add" name="themoi" value="Thêm mới văn bản hành chính">
      </div>
      <table id="tintuc" class="display table table-bordered table-hover">
       <thead>
          <tr>
             <th>ID</th>
             <th>Tiêu đề</th>
             <th>File đính kèm</th>
             <th>Nội dung</th>
             <th>Thao tác</th>
          </tr>
       </thead>
       <tbody>
          <?php
            $sql = "SELECT * FROM vbhanhchinh";
            $result = $conn->query($sql);
            while($tintuc = $result->fetch_assoc())
              {

          ?>
          <tr>
             <th scope="row"><?=$tintuc['id']?></th>
             <td><?=$tintuc['name']?></td>
             <td><?=$tintuc['image']?></td>
             <td class="table-cell"><?=$tintuc['content']?></td>
             <td>
              <button type="button" class="btn btn-primary btn-xs dt-edit" style="margin-right:16px;">
                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-danger btn-xs dt-delete">
                <span class="glyphicon glyphicon-remove dt-delete" aria-hidden="true"></span>
              </button>
            </td>
          </tr>
          <?php
            }
          ?>
       </tbody>
      </table>
    </div>

    <footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <input type="hidden" class="form-control disabled" name="id" id="id">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Tiêu đề</label>
                <input type="text" class="form-control" name="tieude" id="tieude">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Nội dung</label>
                <textarea type="text" class="form-control" name="noidung" id="noidung" rows="10"></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Hình ảnh</label>
                <input type="text" class="form-control" name="hinhanh" id="hinhanh" disabled>
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Tải lên file đính kèm</label>
                <input type="file" id="exampleInputFile" name="fileupload">
                <p class="help-block">Tải lên file đính kèm cho văn bản này</p>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="submit" value="Cập nhật">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#" method="POST" enctype="multipart/form-data">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Tiêu đề</label>
                <input type="text" class="form-control" name="tieudeadd">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Nội dung</label>
                <textarea type="text" class="form-control" name="noidungadd" rows="10"></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Tải lên file đính kèm</label>
                <input type="file" id="exampleInputFile" name="uploadFile">
                <p class="help-block">Tải lên file đính kèm cho hình ảnh này</p>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-info" name="them" value="Thêm mới">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalDelete" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#" method="POST" enctype="multipart/form-data">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Bạn chắc chắn muốn xóa tin này</label>
                <input type="hidden" class="form-control" name="idxoa" id="idxoa">
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-danger" name="xoa" value="Xóa tin này">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
    <script src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>
    <script src='https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js'></script>
    <script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js'></script>
    <script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js'></script>
    <script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js'></script>
    <script src='https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js'></script>
    <script src='https://cdn.datatables.net/buttons/1.2.2/js/buttons.bootstrap.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'></script>
    <script src='https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'></script>
    <script src='https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>
    <script>
      $(document).ready(function() {
        $('#tintuc').DataTable({
          "pagingType": "full_numbers",
          "dom": "Bfrtip",
          "buttons": [
            'csvHtml5',
            'excelHtml5',
            'print'
          ]
        });
      });
    </script>
    <script>
      //Edit row buttons
      $('.dt-edit').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#id').val(dtRow[0].cells[0].innerHTML);
            $('#tieude').val(dtRow[0].cells[1].innerHTML);
            $('#noidung').val(dtRow[0].cells[3].innerHTML);
            $('#hinhanh').val(dtRow[0].cells[2].innerHTML);
            // $('#noibat').val(dtRow[0].cells[5].innerHTML);
            console.log(dtRow[0].cells[i].innerHTML);
          }
          $('#myModal').modal('show');
        });
      });

      //add
      $('.dt-add').each(function () {
        $(this).on('click', function(evt){
          $('#modalAdd').modal('show');
        });
      });

       //delete
      $('.dt-delete').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idxoa').val(dtRow[0].cells[0].innerHTML);
          }
          $('#modalDelete').modal('show');
        });
      });
    </script>
  </body>
</html>
