<?php
include('../config/connect.php');
session_start();
if(isset($_POST['submit']))
{
  $email = $_POST['email'];
  if(isset($_COOKIE['check']))
    $email = "false";
  $password = $_POST['password'];
  $password = md5($password);

  $sql = "SELECT * FROM users WHERE email = '$email' AND password = '$password'";
  $result = $conn->query($sql);

  if($result->num_rows > 0)
  {
    $_SESSION['logsuccess'] = "SUCCESS";
    $sql3 = "SELECT * FROM users WHERE email = '$email'";
    $result3 = $conn->query($sql3);
    while ($users = $result3->fetch_assoc()) {
      $_SESSION['role'] = $users['role'];
      $_SESSION['name'] = $users['name'];
    }
    header("Location: index.php");
  } else {
      $sql2 = "SELECT * FROM users WHERE email = '$email'";
      $result2 = $conn->query($sql2);
          //print_r("<script>alert('"  . $result2 . "</script>");
      while ($users = $result2->fetch_assoc()) {
        $log = $users['log'];
        $log += 1;
        $sql1 = "UPDATE users SET log = '$log' WHERE email = '$email'";
        $result1 = $conn->query($sql1);

        if($users['log']%5 == 0) {
          setcookie('check', 'Bạn đã đăng nhập quá số lần quy định, vui lòng thử lại sau ít phút!', time() + 100);
          //$_COOKIE['logfalsecheck'] = "Bạn đã đăng nhập quá số lần quy định, vui lòng thử lại sau ít phút!";
        }

      }
    $_SESSION['logfalse'] = "Đăng nhập thất bại vui lòng thử lại!";
  }
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trang quản trị</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">CÔNG AN SƠN LA</a>
        </div>
      </div>
    </nav>

    <!-- Begin page content -->
    <div class="container">
      <div class="col-md-6 col-md-push-3" style="margin-top: 100px;">
        <?php
          if(isset($_SESSION['logfalse'])) {
            echo '<div class="alert alert-danger">' . $_SESSION['logfalse'] . "</div>";
          }

          if(isset($_COOKIE['check'])) {
            unset($_SESSION['logfalse']);
            echo '<div class="alert alert-danger">' . $_COOKIE['check'] . "</div>";
          }

        ?>
        <form action="#" method="POST">
          <div class="form-group">
            <label for="exampleInputEmail1">Địa chỉ email</label>
            <input type="text" class="form-control" id="exampleInputEmail1" name="email" placeholder="Email" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Mật khẩu</label>
            <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password" required>
          </div>
          <button type="submit" name="submit" class="btn btn-info" <?php if(isset($_COOKIE['check'])) echo "disabled"; ?>>Đăng nhập quản trị</button>
        </form>
      </div>
    </div>

    <footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
