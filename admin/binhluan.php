<?php
session_start();
  if(!isset($_SESSION['logsuccess'])) {
    header("Location: login.php");
  }
  include('../config/connect.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trang quản trị</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">CASL</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Trang chủ</a></li>
            <li><a href="tintuc.php">Quản lý tin tức</a></li>
            <li><a href="togiac.php">Tin tố giác</a></li>
            <li><a href="anninh.php">Trật tự - An ninh</a></li>
            <li><a href="vbhanhchinh.php">Văn bản hành chính</a></li>
            <li><a href="hoidap.php">Hỏi đáp pháp luật</a></li>
            <li  class="active"><a href="binhluan.php">Bình luận</a></li>
            <li><a href="lienhe.php">Liên hệ</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h1>Bình luận trên bài viết & hỏi đáp</h1>
      </div>
      <table id="table" class="display table table-bordered table-hover">
       <thead>
          <tr>
             <th>ID</th>
             <th>Tên người bình luận</th>
             <th>Địa chỉ email</th>
             <th>Nội dung</th>
             <th>Thể loại</th>
             <th>Thời gian</th>
             <th>Xem chi tiết</th>
          </tr>
       </thead>
       <tbody>
          <?php
            $sql = "SELECT * FROM comments";
            $result = $conn->query($sql);
            while($binhluan = $result->fetch_assoc())
              {
          ?>
          <tr>
             <th scope="row"><?=$binhluan['id']?></th>
             <td><?=$binhluan['name']?></td>
             <td><?=$binhluan['email']?></td>
             <td><?=$binhluan['content']?></td>
             <td><?php echo $binhluan['type'] == 0 ? "Hỏi đáp" : "Tin tức"; ?></td>
             <td><?=$binhluan['created_at']?></td>
             <td><a href="
              <?php
                if($binhluan['type'] == 0)
                  echo "../chitiet-hoidap.php?id=" . $binhluan['idTin'];
                else
                  echo "../chitiet.php?id=" . $binhluan['idTin'];
              ?>">Xem chi tiết</a></td>
          </tr>
          <?php
            }
          ?>
       </tbody>
      </table>
    <footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#table').DataTable();
      });
    </script>
  </body>
</html>
