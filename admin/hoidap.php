<?php
  session_start();
  if(!isset($_SESSION['logsuccess'])) {
    header("Location: login.php");
  }
  include('../config/connect.php');

  if(isset($_POST['submit']))
  {
    $title = $_POST['title'];
    $ask = $_POST['ask'];
    $question = $_POST['question'];

    $sql = "INSERT INTO hoidap(TieuDe, ask, question) VALUES ('$title', '$ask', '$question')";
    $result = $conn->query($sql);

    if($result) {
      $_SESSION['qa-success'] = "Thêm hỏi đáp thành công!";
    } else {
      $_SESSION['false'] = "Lỗi khi thêm hỏi đáp vui lòng thử lại!";
    }
  }

  if(isset($_POST['capnhat']))
  {
    $id = $_POST['id'];
    $tieude = $_POST['tieude'];
    $ask = $_POST['ask'];
    $question =$_POST['question'];

    $result = "UPDATE hoidap SET tieude = '$tieude', ask = '$ask', question = '$question' WHERE id ='$id'";
    $result = $conn->query($result);

    if($result) {
      $_SESSION['qa-success'] = "Sửa hỏi đáp thành công!";
    } else {
      $_SESSION['false'] = "Lỗi khi sửa hỏi đáp vui lòng thử lại!";
    }
  }

  if(isset($_POST['xoahoidap']))
  {
    $id = $_POST['idxoa'];

    $sqlxoacmt = "DELETE FROM comments WHERE idHoiDap = '$id'";
    $resultxoacmt = $conn->query($sqlxoacmt);
    if($resultxoacmt) {
      $result = "DELETE FROM hoidap WHERE id ='$id'";
      $result = $conn->query($result);
    }

    $sql1 = "set @autoid :=0";
    $sql2 = "update hoidap set id = @autoid := (@autoid+1)";
    $sql3 = "alter table hoidap Auto_Increment = 1";
    $conn->query($sql1);
    $conn->query($sql2);
    $conn->query($sql3);

    if($result) {
      $_SESSION['qa-success'] = "Xóa hỏi đáp thành công!";
    } else {
      $_SESSION['false'] = "Lỗi khi xóa hỏi đáp vui lòng thử lại!";
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trang quản trị</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">CASL</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Trang chủ</a></li>
            <li><a href="tintuc.php">Quản lý tin tức</a></li>
            <li><a href="togiac.php">Tin tố giác</a></li>
            <li><a href="anninh.php">Trật tự - An ninh</a></li>
            <li><a href="vbhanhchinh.php">Văn bản hành chính</a></li>
            <li class="active"><a href="hoidap.php">Hỏi đáp pháp luật</a></li>
            <li><a href="binhluan.php">Bình luận</a></li>
            <li><a href="lienhe.php">Liên hệ</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <?php
          if(isset($_SESSION['qa-success']))
            echo '<div class="alert alert-success">' . $_SESSION['qa-success'] . '</div>';
          if(isset($_SESSION['false']))
            echo '<div class="alert alert-success">' . $_SESSION['false'] . '</div>';
        ?>
        <h1>Hỏi đáp pháp luật</h1>
      </div>
      <form action method="POST">
        <div class="form-group">
          <label for="exampleInputEmail1">Nhập tiêu đề</label>
          <input type="text" class="form-control" id="exampleInputEmail1" name="title" placeholder="Tiêu đề" required>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Nhập câu hỏi</label>
          <textarea type="text" class="form-control" id="exampleInputEmail1" name="ask" rows="10" placeholder="Câu hỏi" required></textarea>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Nhập câu trả lời</label>
          <textarea type="text" class="form-control" id="exampleInputEmail1" name="question" rows="10" placeholder="Câu trả lời" required></textarea>
        </div>
        <input type="submit" name="submit" class="btn btn-default" value="Đăng hỏi đáp"></input>
      </form>

      <div class="page-header">
        <h1>Quản lý hỏi đáp</h1>
        <table id="hoidap" class="display table table-bordered table-hover">
         <thead>
            <tr>
               <th>ID</th>
               <th>Tiêu đề</th>
               <th>Câu hỏi</th>
               <th>Trả lời</th>
               <th>Thời gian</th>
               <th>Thao tác</th>
            </tr>
         </thead>
         <tbody>
            <?php
              $sql = "SELECT * FROM hoidap";
              $result = $conn->query($sql);
              while($hoidap = $result->fetch_assoc())
                {

            ?>
            <tr>
               <th scope="row"><?=$hoidap['id']?></th>
               <td><?=$hoidap['TieuDe']?></td>
               <td><?=$hoidap['ask']?></td>
               <td><?=$hoidap['question']?></td>
               <td><?=$hoidap['created_at']?></td>
               <td>
                <button type="button" class="btn btn-primary btn-xs dt-edit" style="margin-right:16px;">
                  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </button>
                <button type="button" class="btn btn-danger btn-xs dt-delete">
                  <span class="glyphicon glyphicon-remove dt-delete" aria-hidden="true"></span>
                </button>
              </td>
            </tr>
            <?php
              }
            ?>
         </tbody>
      </table>
      </div>
    </div>

    <footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>

    <!-- Modal -->
    <div id="modalEdit" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <input type="hidden" class="form-control disabled" name="id" id="id">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Tiêu đề</label>
                <input type="text" class="form-control" name="tieude" id="tieude">
              </div>
               <div class="form-group">
                <label for="exampleInputPassword1">Câu hỏi</label>
                <textarea type="text" class="form-control" name="ask" id="ask" rows="10"></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Câu trả lời</label>
                <textarea type="text" class="form-control" name="question" id="question" rows="10"></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-success" name="capnhat" value="Cập nhật">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    <!-- Modal -->
    <div id="modalRemove" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <input type="hidden" class="form-control disabled" name="idxoa" id="idxoa">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Bạn có chắc chắn muốn xóa bản tin này?</label>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-danger" name="xoahoidap" value="Xóa bản tin này">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#hoidap').DataTable();
      });

      //Edit row buttons
      $('.dt-edit').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#id').val(dtRow[0].cells[0].innerHTML);
            $('#tieude').val(dtRow[0].cells[1].innerHTML);
            $('#ask').val(dtRow[0].cells[2].innerHTML);
            $('#question').val(dtRow[0].cells[3].innerHTML);
            console.log(dtRow[0].cells[i].innerHTML);
          }
          $('#modalEdit').modal('show');
        });
      });

      //add
      $('.dt-delete').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idxoa').val(dtRow[0].cells[0].innerHTML);
          }
          $('#modalRemove').modal('show');
        });
      });
    </script>
  </body>
</html>
