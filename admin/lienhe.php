<?php
  include('../config/connect.php');
  session_start();
  if(!isset($_SESSION['logsuccess'])) {
    header("Location: login.php");
  }

  if(isset($_POST['xoa']))
  {
    $idxoa = $_POST['idxoa'];
    $sql = "DELETE FROM lienhe WHERE id = '$idxoa'";
    $result = $conn->query($sql);

    if($result) {
      $_SESSION['xoa'] = "Đã xóa bản ghi thành công!";
    } else {
      $_SESSION['xoafalse'] = "Có lỗi xảy ra không thể xóa bản ghi!";
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trang quản trị</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">CASL</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Trang chủ</a></li>
            <li><a href="tintuc.php">Quản lý tin tức</a></li>
            <li><a href="togiac.php">Tin tố giác</a></li>
            <li><a href="anninh.php">Trật tự - An ninh</a></li>
            <li><a href="vbhanhchinh.php">Văn bản hành chính</a></li>
            <li><a href="hoidap.php">Hỏi đáp pháp luật</a></li>
            <li><a href="binhluan.php">Bình luận</a></li>
            <li class="active"><a href="lienhe.php">Liên hệ</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h1>Các yêu cầu liên hệ</h1>
        <?php
          if(isset($_SESSION['xoa']))
            echo '<div class="alert alert-success">' . $_SESSION['xoa'] . '</div>';
          if(isset($_SESSION['xoafalse']))
            echo '<div class="alert alert-danger">' . $_SESSION['xoafalse'] . '</div>';
        ?>
      </div>
      <table id="table" class="display table table-bordered table-hover">
       <thead>
          <tr>
             <th>ID</th>
             <th>Tiêu đề</th>
             <th>Họ và tên</th>
             <th>Email</th>
             <th>Số điện thoại</th>
             <th>Địa chỉ</th>
             <th>Nội dung</th>
             <th>Thao tác</th>
          </tr>
       </thead>
       <tbody>
          <?php
            $sql = "SELECT * FROM lienhe";
            $result = $conn->query($sql);
            while($lienhe = $result->fetch_assoc())
              {
          ?>
          <tr>
             <th scope="row"><?=$lienhe['id']?></th>
             <td><?=$lienhe['tieude']?></td>
             <td><?=$lienhe['hoten']?></td>
             <td><?=$lienhe['email']?></td>
             <td><?=$lienhe['dienthoai']?></td>
             <td><?=$lienhe['diachi']?></td>
             <td><?=$lienhe['noidung']?></td>
             <td class="dt-delete"><a href="#">Xóa</a></td>
          </tr>
          <?php
            }
          ?>
       </tbody>
      </table>
    </div>

    <footer class="footer">
      <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
      </div>
    </footer>

    <!-- Modal -->
    <div id="modalRemove" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <input type="hidden" class="form-control disabled" name="idxoa" id="idxoa">
                <label for="exampleInputEmail1">Bạn chắc chắn muốn xóa bản ghi này?</label>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-danger" name="xoa" value="Xóa bản ghi">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#table').DataTable();
      });

      //remove
      $('.dt-delete').each(function () {
        $(this).on('click', function(evt){
          $this = $(this);
          var dtRow = $this.parents('tr');
          for(var i=0; i < dtRow[0].cells.length; i++){
            $('#idxoa').val(dtRow[0].cells[0].innerHTML);
          }
          $('#modalRemove').modal('show');
        });
      });
    </script>
  </body>
</html>
