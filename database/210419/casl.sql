-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 21, 2019 lúc 07:24 AM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `casl`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idTin` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idHoiDap` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `comments`
--

INSERT INTO `comments` (`id`, `name`, `idTin`, `email`, `idHoiDap`, `content`, `type`, `created_at`) VALUES
(22, 'Trần Hồng', 1, 'hongtran1123@gmail.com', 1, 'Bài viết rất hữu ích', 1, '2019-04-21 04:38:50'),
(23, 'Thủy Nguyễn', 1, 'nguyyenthuy1123@gmail.com', 1, 'Hỏi đáp pháp luật rất hữu ích', 0, '2019-04-21 04:39:48'),
(24, 'Trần Hoàng', 2, 'hoangtran@gmail.com', 1, 'Tin tức hữu ích', 1, '2019-04-21 04:56:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoidap`
--

CREATE TABLE `hoidap` (
  `id` int(11) UNSIGNED NOT NULL,
  `TieuDe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ask` text COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `hoidap`
--

INSERT INTO `hoidap` (`id`, `TieuDe`, `ask`, `question`, `created_at`) VALUES
(1, 'XÓA ÁN TÍCH THEO QUY ĐỊNH CỦA BỘ LUẬT HÌNH SỰ NĂM 2015?', 'Luật sư cho tôi hỏi quy định về xóa án tích theo quy định của Bộ luật hình sự năm 2015 được quy định như thế nào? rất mong luật sư tư vấn giúp tôi, cảm ơn luật sư!', 'Với những thông tin bạn cung cấp bộ phận tư vấn luật đưa ra ý kiến tư vấn cho bạn như sau:Căn cứ tại Điều 107 Bộ luật hình sự năm 2015 có quy định về  Xóa án tích“1. Người dưới 18 tuổi bị kết án được coi là không có án tích, nếu thuộc một trong các trường hợp sau đây:a) Người từ đủ 14 đến dưới 16 tuổi;b) Người từ đủ 16 tuổi đến dưới 18 tuổi bị kết án về tội phạm ít nghiêm trọng, tội phạm nghiêm trọng hoặc tội phạm rất nghiêm trọng do vô ý;c) Người bị áp dụng biện pháp tư pháp quy định tại Mục 3 Chương này.2. Người từ đủ 16 đến dưới 18 tuổi bị kết án về tội phạm rất nghiêm trọng do cố ý hoặc tội phạm đặc biệt nghiêm trọng thì đương nhiên xóa án tích nếu trong thời hạn 03 năm tính từ khi chấp hành xong hình phạt chính hoặc từ khi hết thời hiệu thi hành bản án mà người đó không thực hiện hành vi phạm tội mới.”Như vậy, theo quy định trên tại Khoản 1 các trường hợp bị kết án theo quy định trên thì không coi là có án tích, việc bị kết án này thì không bị sử dụng để tính tái phạm, tái phạm nguy hiểm nếu sau lần phạm tội này người đó lại phạm tội khác. Quy định này mở rộng phạm vi những trường hợp người dưới 18 tuổi phạm tội không bị coi là có án tích so với quy định của Bộ luật hình sự năm 1999.', '2019-04-16 10:08:04');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lienhe`
--

CREATE TABLE `lienhe` (
  `id` int(11) NOT NULL,
  `tieude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hoten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dienthoai` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diachi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `lienhe`
--

INSERT INTO `lienhe` (`id`, `tieude`, `hoten`, `email`, `dienthoai`, `diachi`, `noidung`) VALUES
(2, 'Nguyễn Văn A', 'Nguyễn Văn A', 'tiensoul.ask@gmail.com', '0971736217', 'DSD', 'Thông tin về người báo tin không bắt buộc phải cung cấp. Nếu người báo tin cung cấp thông tin cá nhân; các thông tin này sẽ được giữ bí mật và đảm bảo chỉ sử dụng vào mục đích phòng chống tội phạm.');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `theloai`
--

CREATE TABLE `theloai` (
  `idTheLoai` int(10) UNSIGNED NOT NULL,
  `Ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TenKhongDau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `theloai`
--

INSERT INTO `theloai` (`idTheLoai`, `Ten`, `TenKhongDau`, `created_at`, `updated_at`) VALUES
(1, 'Xã Hội', 'Xa-Hoi', NULL, NULL),
(2, 'Thế Giới', 'The-Gioi', NULL, NULL),
(3, 'Kinh Doanh', 'Kinh-Doanh', NULL, NULL),
(4, 'Văn Bản Hành Chính', 'Van-Ban-Hanh-Chinh', NULL, NULL),
(5, 'Thể Thao', 'The-Thao', NULL, NULL),
(6, 'Trật Tự An Ninh', 'Trat-Tu-An-Ninh', NULL, NULL),
(7, 'Đời Sống', 'Doi-Song', NULL, NULL),
(8, 'Khoa Học', 'Khoa-Hoc', NULL, NULL),
(9, 'Vi Tính', 'Vi-Tinh', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tintuc`
--

CREATE TABLE `tintuc` (
  `id` int(10) UNSIGNED NOT NULL,
  `TieuDe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TieuDeKhongDau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TomTat` text COLLATE utf8_unicode_ci NOT NULL,
  `NoiDung` longtext COLLATE utf8_unicode_ci NOT NULL,
  `Hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NoiBat` int(11) NOT NULL DEFAULT '0',
  `SoLuotXem` int(11) DEFAULT '0',
  `idLoaiTin` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tintuc`
--

INSERT INTO `tintuc` (`id`, `TieuDe`, `TieuDeKhongDau`, `TomTat`, `NoiDung`, `Hinh`, `NoiBat`, `SoLuotXem`, `idLoaiTin`, `created_at`, `updated_at`) VALUES
(1, 'Nghệ An: Đường dây ma túy “khủng” và hành trình tẩu tán tang vật', '', 'Sau khi tạm giữ 3 đối tượng, truy nã 3 đối tượng người nước ngoài bỏ trốn trong đường dây vận chuyển gần 500kg ma túy đá tại huyện Quỳnh Lưu (Nghệ An), cơ quan điều tra Công an tỉnh Nghệ An đã ra quyết định khởi tố vụ án hình sự về tội Vận chuyển trái phép chất ma túy, Không tố giác tội phạm và Cưỡng đoạt tài sản.', 'Hình thành đường dây ma túy xuyên quốc gia\r\n\r\nSau khi bắt giữ các đối tượng và thu giữ gần 500kg ma túy đá tại địa bàn xã Quỳnh Thuận, huyện Quỳnh Lưu (Nghệ An), bước đầu các đối tượng đã khai nhận hành vi phạm tội của mình.\r\n\r\nTheo lời khai của Nguyên Văn Phú, sau khoảng thời gian lao động ở Đài Loan từ năm 2013-2017, Phú có quen biết một người đàn ông tên Quang (hiện đang làm việc tại Đài Loan). Đến tháng 7.2017, sau khi hết thời hạn lao động, Phú trở về quê sinh sống, làm ăn.\r\n\r\nĐến đầu năm 2019, Quang từ Đài Loan liên lạc qua điện thoại nhờ Phú làm phiên dịch cho một số người Đài Loan sang Việt Nam du lịch và làm ăn. Thấy việc dễ dàng lại được giao lưu với người nước ngoài, Phú gật đầu đồng ý.\r\n\r\nĐầu tháng 3.2019, một số đối tượng người Đài Loan sang Việt Nam gặp và nói Phú tìm thuê giúp một căn nhà kho để xe ô tô và hàng hóa tại khu vực huyện Quỳnh Lưu.\r\nRất nhanh sau đó, Nguyễn Văn Phú đã tìm và thuê được một kho hàng của chị Nguyễn Thị Tâm (SN 1977) trú tại xóm 1, xã Quỳnh Hồng, huyện Quỳnh Lưu. Đến đầu tháng 4.2019, Phú nhận 25 thùng cát tông màu vàng từ một xe chở hàng biển kiểm soát Lào và cất trong kho hàng đã thuê.\r\n\r\nVài ngày sau, một người có quốc tịch Đài Loan gọi Phú về việc lô hàng bị lỗi và cử người sang để sửa. Sau đó có 3 người Đài Loan gồm Lin Sheng Hsiun (SN 1976); Lin Kun Jund (SN 1978) và Ho Yu-Hsiang (SN 1998) sang gặp Phú để sửa lô hàng nói trên.\r\n\r\nKhi mở các thùng cát tông, Phú thấy có 50 loa thùng màu đen, loại kéo di động. Sau đó chỉ có 2 người Đài Loan đóng cửa kho và làm việc bên trong. Riêng Phú và một người Đài Loan nữa ngược về TP.Vinh thuê khách sạn Mường Thanh Sông Lam nghỉ ngơi và chờ đồng đội.\r\nĐến chiều tối 15.4, khi báo chí thông tin việc Công an Nghệ An phối hợp Cục phòng chống tội phạm về ma túy và Công an Hà Tĩnh khám xét nhà P.V.P (SN 1953, số nhà 7, ngõ 161, đường Phùng Chí Kiên – Khối 17, phường Hà Huy Tập, TP.Vinh, Nghệ An) và thu giữ 600kg ma túy được cất giấu trong loa thùng, giống các loa thùng tại kho hàng Phú đã thuê. Phú thông báo cho một đối tượng người Đài Loan biết thông tin thì đối tượng nói đường dây ma túy bị lộ, Phú phải chịu trách nhiệm đem đi vứt tất cả số hàng trên.\r\n\r\nHành trình tẩu tán gần 500kg ma túy đá\r\n\r\nNgay sau khi nhận được lệnh từ đối tượng người Đài Loan, Nguyễn Văn Phú đã gọi cho bạn là Nguyễn Bảo Trung (SN 1996, trú tại xã Hưng Đông, TP.Vinh) để nhờ kiếm xe chở hàng. Thấy vậy, Trung gọi điện cho Nguyễn Đức Bút (SN 1991, trú tại xã Nghi Kim, TP.Vinh) đến gặp Phú bàn bạc.\r\n\r\nTại đây, Bút giới thiệu Phú đến nhà Võ Sỹ Mạnh (SN 1983, trú tại xã Nghi Kim, TP.Vinh) để thuê Mạnh chở Bút, Phú và Trung đến kho chở hàng ở xã Quỳnh Hồng, huyện Quỳnh Lưu.\r\n\r\nKhi tới kho hàng, Phú nói Trung kéo các bao tải màu trắng ở phía trong kho ra bên ngoài, sau đó, Phú trực tiếp bốc các bao tải lên xe, còn Mạnh lên thùng xe hỗ trợ xếp hàng. Bút không tham gia bốc hàng.\r\n\r\nGần sáng 16.4, sau khi bốc các bao tải lên thùng xe, các đối tượng đã di chuyển trên tuyến quốc lộ 48B để ra khỏi địa bàn tỉnh Nghệ An. Tuy nhiên, do nghi ngờ có lực lượng công an nên các đối tượng đổ số hàng trên tại khu vực cánh đồng muối thuộc xã Quỳnh Thuận, huyện Quỳnh Lưu.\r\n\r\nTrong vụ án này, Nguyễn Văn Phú đóng vai trò là đối tượng cầm đầu, chủ mưu trong vụ vận chuyển, tẩu tán gần 500kg ma túy đá.\r\n\r\nĐối tượng Nguyễn Bảo Trung khai nhận biết số hàng mà Phú vận chuyển là ma túy kể từ khi đến kho hàng, vì Trung thấy các thùng cát tông, loa thùng, bao tải màu trắng giống như vụ ma túy đã bị bắt giữ tại đường Phùng Chí Kiên, phường Hà Huy Tập, TP.Vinh, nhưng Trung không nói gì và vẫn đồng ý giúp Phú.\r\n\r\nRiêng Võ Sỹ Mạnh, sau khi vận chuyển thuê cho Phú, Mạnh đã nghi ngờ số hàng trên là hàng cấm nên gọi điện thoại uy hiếp, đe dọa Nguyễn Văn Phú và yêu cầu Phú chuyển 9 triệu đồng cho Mạnh. Phú lo sợ nên đã đồng ý chuyển tiền cho Mạnh.\r\n\r\nĐối với 3 người đàn ông mang quốc tịch Đài Loan đã xuất cảnh khỏi Việt Nam trong các ngày 15, 16.4, hiện cơ quan công an đang truy nã quốc tế.', 'Nghe-An-duong-day-ma-tuy-khung-va-hanh-trinh-tau-tan-tang-vat-a-ph---1-1555817768-width800height523.jpg', 1, 0, 6, '2019-04-21 04:34:55', '2019-04-21 04:34:55'),
(2, 'Quảng Bình: Triệt phá đường dây ma tuý và mại dâm tại quán karaoke, khách sạn', '', '(PLVN) - Lực lượng chức năng Công an tỉnh Quảng Bình vừa tổ chức triệt phá đường dây sử dụng chất ma tuý và mại dâm tại quán karaoke, khách sạn. Phát hiện 43 đối tượng dương tính với ma tuý, 4 đối tượng đang tổ chức mua bán dâm.', 'Vào khoảng 2h, ngày 19/4, hơn 120 cán bộ, chiến sĩ công an ở các đơn vị thuộc lực lượng Công an tỉnh Quảng Bình đã tổ chức triệt phá thành công chuyên án mang bí số 328-T, về đường dây sử dụng trái phép chất ma tuý và mại dâm tại quán karaoke, khách sạn.\r\n\r\nTheo đó, bốn tổ công tác đã tiến hành kiểm tra đột xuất quán karaoke Thiên Đường II và 3 khách sạn trên địa bàn TP Đồng Hới(Quảng Bình). Qua kiểm tra, lực lượng chức năng phát hiện 43 đối tượng dương tính với ma tuý và 4 đối tượng đang tổ chức mua bán dâm.\r\n\r\nCụ thể, tại quán karaoke Thiên Đường II (phường Đồng Phú, TP Đồng Hới), lực lượng chức năng tiến hành kiểm tra và phát hiện, bốn phòng có 33 đối tượng (21 nam, 12 nữ) dương tính với ma tuý. Đồng thời, thu giữ nhiều gói nhỏ chứa chất màu trắng dạng đá và một số viên nén màu hồng nghi là ma túy và nhiều dụng cụ để sử dụng ma túy như đĩa và ống hút…\r\nTại khách sạn Osaka (xã Bảo Ninh, TP Đồng Hới), lực lượng chức năng phát hiện 10 đối tượng (5 nam, 5 nữ) đang bay lắc. Qua kiểm tra, tất cả đều dương tính với chất ma tuý. Còn tại khách sạn Phú Cường (xã Quang Phú, TP Đồng Hới) và khách sạn White Rose (phường Đồng Phú, tp.Đồng Hới) cơ quan chức năng phát hiện 2 đôi nam nữ có hành vi mua bán dâm.\r\n\r\nHiện, vụ việc đang tiếp tục được các cơ quan chức năng điều tra làm rõ.', '5837680611989458436164823053145113736773632n_hxwt.jpg', 1, 0, 6, '2019-04-21 04:36:13', '2019-04-21 04:36:13');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `togiac`
--

CREATE TABLE `togiac` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `timeaction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `togiac`
--

INSERT INTO `togiac` (`id`, `name`, `title`, `phone`, `timeaction`, `location`, `content`) VALUES
(1, 'Nguyễn Văn A', 'Xin chào', '971736217', '2019-04-12', 'tai nha', 'tin to cao'),
(2, 'Nguyễn Văn A', 'Xin chào', '971736217', '2019-04-04', 'tai nha', 'tin to giac 2');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `log` int(11) DEFAULT '0',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `log`, `password`, `created_at`, `updated_at`) VALUES
(17, 'Tiến Minh', 'tienlee.ask@gmail.com', 1, 48, '7f834d91e4ea1068bd6d4fcf6098bd6a', '2019-04-18 10:50:45', NULL),
(26, 'Hương Hương', 'huongtk@gmail.com', 0, 0, '7f834d91e4ea1068bd6d4fcf6098bd6a', '2019-04-19 14:31:51', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vbhanhchinh`
--

CREATE TABLE `vbhanhchinh` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `vbhanhchinh`
--

INSERT INTO `vbhanhchinh` (`id`, `name`, `image`, `content`, `created_at`) VALUES
(1, 'QUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU update', 'square-gopher.png', 'QUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU update', '2019-04-18 09:23:18'),
(2, 'QUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU update', 'square-gopher.png', 'QUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU update', '2019-04-18 09:23:18');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tintuc` (`idTin`),
  ADD KEY `fk_hoidap` (`idHoiDap`);

--
-- Chỉ mục cho bảng `hoidap`
--
ALTER TABLE `hoidap`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `lienhe`
--
ALTER TABLE `lienhe`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `theloai`
--
ALTER TABLE `theloai`
  ADD PRIMARY KEY (`idTheLoai`);

--
-- Chỉ mục cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tintuc_idloaitin_foreign` (`idLoaiTin`);

--
-- Chỉ mục cho bảng `togiac`
--
ALTER TABLE `togiac`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Chỉ mục cho bảng `vbhanhchinh`
--
ALTER TABLE `vbhanhchinh`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `hoidap`
--
ALTER TABLE `hoidap`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `lienhe`
--
ALTER TABLE `lienhe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `theloai`
--
ALTER TABLE `theloai`
  MODIFY `idTheLoai` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `togiac`
--
ALTER TABLE `togiac`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `vbhanhchinh`
--
ALTER TABLE `vbhanhchinh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_hoidap` FOREIGN KEY (`idHoiDap`) REFERENCES `hoidap` (`id`),
  ADD CONSTRAINT `fk_tintuc` FOREIGN KEY (`idTin`) REFERENCES `tintuc` (`id`);

--
-- Các ràng buộc cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  ADD CONSTRAINT `fk_theloai` FOREIGN KEY (`idLoaiTin`) REFERENCES `theloai` (`idTheLoai`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
