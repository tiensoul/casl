-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 19, 2019 lúc 06:22 PM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `casl`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idTin` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `comments`
--

INSERT INTO `comments` (`id`, `name`, `idTin`, `email`, `content`, `created_at`) VALUES
(8, 'Thu Thủy', 980, 'thuythu@gmail.com', 'tin rất hữu ích', '2019-04-19 16:20:38');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `commentshd`
--

CREATE TABLE `commentshd` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idTin` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `commentshd`
--

INSERT INTO `commentshd` (`id`, `name`, `idTin`, `email`, `content`, `created_at`) VALUES
(2, 'Nguyễn Hòa', 1, 'hoanguyen@gmail.com', 'bài viết rất hữu ích', '2019-04-19 16:20:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoidap`
--

CREATE TABLE `hoidap` (
  `id` int(11) NOT NULL,
  `TieuDe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ask` text COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `hoidap`
--

INSERT INTO `hoidap` (`id`, `TieuDe`, `ask`, `question`, `created_at`) VALUES
(1, 'XÓA ÁN TÍCH THEO QUY ĐỊNH CỦA BỘ LUẬT HÌNH SỰ NĂM 2015?', 'Luật sư cho tôi hỏi quy định về xóa án tích theo quy định của Bộ luật hình sự năm 2015 được quy định như thế nào? rất mong luật sư tư vấn giúp tôi, cảm ơn luật sư!', 'Với những thông tin bạn cung cấp bộ phận tư vấn luật đưa ra ý kiến tư vấn cho bạn như sau:Căn cứ tại Điều 107 Bộ luật hình sự năm 2015 có quy định về  Xóa án tích“1. Người dưới 18 tuổi bị kết án được coi là không có án tích, nếu thuộc một trong các trường hợp sau đây:a) Người từ đủ 14 đến dưới 16 tuổi;b) Người từ đủ 16 tuổi đến dưới 18 tuổi bị kết án về tội phạm ít nghiêm trọng, tội phạm nghiêm trọng hoặc tội phạm rất nghiêm trọng do vô ý;c) Người bị áp dụng biện pháp tư pháp quy định tại Mục 3 Chương này.2. Người từ đủ 16 đến dưới 18 tuổi bị kết án về tội phạm rất nghiêm trọng do cố ý hoặc tội phạm đặc biệt nghiêm trọng thì đương nhiên xóa án tích nếu trong thời hạn 03 năm tính từ khi chấp hành xong hình phạt chính hoặc từ khi hết thời hiệu thi hành bản án mà người đó không thực hiện hành vi phạm tội mới.”Như vậy, theo quy định trên tại Khoản 1 các trường hợp bị kết án theo quy định trên thì không coi là có án tích, việc bị kết án này thì không bị sử dụng để tính tái phạm, tái phạm nguy hiểm nếu sau lần phạm tội này người đó lại phạm tội khác. Quy định này mở rộng phạm vi những trường hợp người dưới 18 tuổi phạm tội không bị coi là có án tích so với quy định của Bộ luật hình sự năm 1999.', '2019-04-16 10:08:04');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lienhe`
--

CREATE TABLE `lienhe` (
  `id` int(11) NOT NULL,
  `tieude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hoten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dienthoai` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diachi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `lienhe`
--

INSERT INTO `lienhe` (`id`, `tieude`, `hoten`, `email`, `dienthoai`, `diachi`, `noidung`) VALUES
(2, 'Nguyễn Văn A', 'Nguyễn Văn A', 'tiensoul.ask@gmail.com', '0971736217', 'DSD', 'Thông tin về người báo tin không bắt buộc phải cung cấp. Nếu người báo tin cung cấp thông tin cá nhân; các thông tin này sẽ được giữ bí mật và đảm bảo chỉ sử dụng vào mục đích phòng chống tội phạm.');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide`
--

CREATE TABLE `slide` (
  `id` int(10) UNSIGNED NOT NULL,
  `Ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slide`
--

INSERT INTO `slide` (`id`, `Ten`, `Hinh`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Phan Thị Mơ thử sức với sân khấu kịch', 'phan-thi-mo-1-3984-1490342174_490x294.jpg', 'phan-thi-mo-thu-suc-voi-san-khau-kich', NULL, NULL),
(2, 'Phạm Hương khoe giọng trong MV đầu tay', 'pham-huong-khoe-giong-trong-mv-dau-tay-1491286120_490x294.jpg', 'pham-huong-khoe-giong-trong-mv-dau-tay', NULL, NULL),
(3, 'Ngọc Trinh khác lạ với màu tóc mới', 'ngoc-trinh-khoe-hinh-anh-khac-la-voi-toc-moi-nhuom-1491274712_490x294.jpg', 'ngoc-trinh-khac-la-voi-mau-toc-moi', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `theloai`
--

CREATE TABLE `theloai` (
  `idTheLoai` int(10) UNSIGNED NOT NULL,
  `Ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TenKhongDau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `theloai`
--

INSERT INTO `theloai` (`idTheLoai`, `Ten`, `TenKhongDau`, `created_at`, `updated_at`) VALUES
(1, 'Xã Hội', 'Xa-Hoi', NULL, NULL),
(2, 'Thế Giới', 'The-Gioi', NULL, NULL),
(3, 'Kinh Doanh', 'Kinh-Doanh', NULL, NULL),
(4, 'Văn Bản Hành Chính', 'Van-Ban-Hanh-Chinh', NULL, NULL),
(5, 'Thể Thao', 'The-Thao', NULL, NULL),
(6, 'Trật Tự An Ninh', 'Trat-Tu-An-Ninh', NULL, NULL),
(7, 'Đời Sống', 'Doi-Song', NULL, NULL),
(8, 'Khoa Học', 'Khoa-Hoc', NULL, NULL),
(9, 'Vi Tính', 'Vi-Tinh', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tintuc`
--

CREATE TABLE `tintuc` (
  `id` int(10) UNSIGNED NOT NULL,
  `TieuDe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TieuDeKhongDau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TomTat` text COLLATE utf8_unicode_ci NOT NULL,
  `NoiDung` longtext COLLATE utf8_unicode_ci NOT NULL,
  `Hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NoiBat` int(11) NOT NULL DEFAULT '0',
  `SoLuotXem` int(11) DEFAULT '0',
  `idLoaiTin` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tintuc`
--

INSERT INTO `tintuc` (`id`, `TieuDe`, `TieuDeKhongDau`, `TomTat`, `NoiDung`, `Hinh`, `NoiBat`, `SoLuotXem`, `idLoaiTin`, `created_at`, `updated_at`) VALUES
(980, 'Bắt nhóm người Trung Quốc buôn 1,1 tấn ma túy ở Sài Gòn', '', 'Từ việc phát hiện những chiếc ôtô đậu sai làn đường, Công an TP.HCM đã phá được đường dây buôn bán ma túy lớn, thu giữ hơn 1,1 tấn ma túy.', 'Khuya 19/4, Công an TP.HCM cho biết vừa triệt phá chuyên án ma túy lớn do nhóm người Trung Quốc cầm đầu. Bước đầu, số ma túy bị thu giữ hơn 1,1 tấn.\r\n\r\nTrước đó, khuya 12/4, qua camera giám sát trật tự giao thông, Đội Cảnh sát giao thông Công an quận 5 phát hiện ôtô 7 chỗ do Yeh Ching Wei (33 tuổi) điều khiển chở Chiang Wei Chih (31 tuổi, cùng quốc tịch Đài Loan), và 2 ôtô tải đang đậu đỗ sai quy định có biểu hiện nghi vấn.', '1f3a9503505cb502ec4d.jpg', 1, 0, 6, '2019-04-19 16:11:05', '2019-04-19 16:11:05');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `togiac`
--

CREATE TABLE `togiac` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `timeaction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `togiac`
--

INSERT INTO `togiac` (`id`, `name`, `title`, `phone`, `timeaction`, `location`, `content`) VALUES
(1, 'Nguyễn Văn A', 'Xin chào', '971736217', '2019-04-12', 'tai nha', 'tin to cao'),
(2, 'Nguyễn Văn A', 'Xin chào', '971736217', '2019-04-04', 'tai nha', 'tin to giac 2');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `log` int(11) DEFAULT '0',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `log`, `password`, `created_at`, `updated_at`) VALUES
(17, 'Tiến Minh', 'tienlee.ask@gmail.com', 1, 48, '7f834d91e4ea1068bd6d4fcf6098bd6a', '2019-04-18 10:50:45', NULL),
(26, 'Hương Hương', 'huongtk@gmail.com', 0, 0, '7f834d91e4ea1068bd6d4fcf6098bd6a', '2019-04-19 14:31:51', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vbhanhchinh`
--

CREATE TABLE `vbhanhchinh` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `vbhanhchinh`
--

INSERT INTO `vbhanhchinh` (`id`, `name`, `image`, `content`, `created_at`) VALUES
(1, 'QUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU update', 'square-gopher.png', 'QUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU update', '2019-04-18 09:23:18'),
(2, 'QUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU update', 'square-gopher.png', 'QUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU\r\nQUYẾT ĐỊNH SỐ 353/QĐ-CAT-PV01 NGÀY 28/8/2018 CỦA GIÁM ĐỐC CÔNG AN TỈNH SƠN LA VỀ CÔNG BỐ TRÊN TRANG THÔNG TIN ĐIỆN TỬ CÔNG AN TỈNH SƠN LA CÁC THỦ TỤC HÀNH CHÍNH THUỘC LĨNH VỰC QUẢN LÝ NGÀNH, NGHỀ ĐẦU TƯ KINH DOANH CÓ ĐIỀU KIỆN VỀ ANTT; QUẢN LÝ VŨ KHÍ VẬT LIỆU NỔ VÀ CCHT; ĐĂNG KÝ, QUẢN LÝ CON DẤU update', '2019-04-18 09:23:18');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `commentshd`
--
ALTER TABLE `commentshd`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `hoidap`
--
ALTER TABLE `hoidap`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `lienhe`
--
ALTER TABLE `lienhe`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `theloai`
--
ALTER TABLE `theloai`
  ADD PRIMARY KEY (`idTheLoai`);

--
-- Chỉ mục cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tintuc_idloaitin_foreign` (`idLoaiTin`);

--
-- Chỉ mục cho bảng `togiac`
--
ALTER TABLE `togiac`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Chỉ mục cho bảng `vbhanhchinh`
--
ALTER TABLE `vbhanhchinh`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `commentshd`
--
ALTER TABLE `commentshd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `hoidap`
--
ALTER TABLE `hoidap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `lienhe`
--
ALTER TABLE `lienhe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `theloai`
--
ALTER TABLE `theloai`
  MODIFY `idTheLoai` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=981;

--
-- AUTO_INCREMENT cho bảng `togiac`
--
ALTER TABLE `togiac`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `vbhanhchinh`
--
ALTER TABLE `vbhanhchinh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
